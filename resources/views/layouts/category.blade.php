<div class="col-sm-3">
    <div class="left-sidebar">
        <h2>Каталог товаров</h2>
        <div class="panel panel-default category-products">

            <!--ВЫВОД КАТЕГОРИЙ КАТАЛОГА-->
            @foreach($categoryList as $category)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        @if(isset($categorySelected))
                        <a class="
                        {{ $categorySelected->id == $category->id? 'selected' : '' }}" href="/category/{{ $category->id }}">
                            {{ $category->name }}
                        </a>
                        @else
                        <a href="/category/{{$category->id}}">{{ $category->name }}</a>
                        @endif
                    </h4>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</div>