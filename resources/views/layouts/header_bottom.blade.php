<div class="header-bottom"><!--header-bottom-->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainmenu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="mainmenu pull">
                    <ul class="nav navbar-nav collapse navbar-collapse" id="mainmenu">
                        <li><a href="/">Главная</a></li>
                        <li class="dropdown"><a href="/">Магазин<span class="caret"></span></a>
                            <ul role="menu" class="sub-menu">
                                <li><a href="/catalog">Каталог товаров</a></li>
                                <li><a href="/cart">Корзина</a></li>
                            </ul>
                        </li>
                        <li><a href="/contacts">Контакты</a></li>
                        <li><a href="/about">Доставка и оплата</a></li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li><a href="/cart"><i class="fa fa-shopping-cart fa-lg"></i> Корзина
                                <span id="cart-count" class="active">({{ $count or 0 }})</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/header-bottom-->