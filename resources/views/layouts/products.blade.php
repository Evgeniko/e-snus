@foreach($products as $product)
    <div class="col-xs-12 col-sm-4 product-card">
        <div class="product-image-wrapper panel panel-default">
            <div class="single-products">
                <div class="productinfo text-center">
                    <a href="/product/{{ $product->id }}" >
                        <img src="{!! $product->image !!}" alt="{{ $product->name }}" />
                    </a>
                    <h2>{{ $product->price }} руб.</h2>
                    <p>
                        <a href="/product/{{ $product->id }}" >
                            {{ $product->name }}
                        </a>
                    </p>
                    <p>
                        <select class="rating-show">
                            <option value="1"{{$product->rating == 1? 'selected':''}}>1</option>
                            <option value="2"{{$product->rating == 2? 'selected':''}}>2</option>
                            <option value="3"{{$product->rating == 3? 'selected':''}}>3</option>
                            <option value="4"{{$product->rating == 4? 'selected':''}}>4</option>
                            <option value="5"{{$product->rating == 5? 'selected':''}}>5</option>
                        </select>
                    </p>
                    <div class="product_attrib_wrap">
                        <div class="product_attrib can_amount">
                            <strong>{{ $product->pouches }}</strong><br><label>Пакетиков</label>
                        </div>
                        <div class="product_attrib nicotine">
                            <strong>{{ $product->nicotine }}</strong> мг/г<br><label>Никотин</label>
                        </div>
                        <div class="product_attrib weight">
                            <strong>{{ $product->wp }}</strong> г<br><label>Вес Пакетика</label>
                        </div>
                    </div>
                    <a href="#" data-id="{{ $product->id }}"
                       onclick="yaCounter44026404.reachGoal('vkorzinu'); return false;"
                       class="btn btn-default btn-sm add-to-cart"><i class="fa fa-cart-plus" aria-hidden="true"></i>В корзину
                    </a>
                </div>
                <div class="stickers">
                    <div class="{{ $product->stock == 'Под заказ'? 'ordering' : 'availability' }}">
                        {{ $product->stock }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
