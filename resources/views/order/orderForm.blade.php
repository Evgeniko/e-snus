@extends('layouts.app')
@section('header_bottom')
    @include('layouts.header_bottom')
@endsection
@section('content')
    <section>
        <div class="container">


            <div class="col-sm-8 padding-right col-sm-offset-2">
                <div class="features_items">
                    <h2 class="title text-center">Оформление покупки</h2>

                    @if(session()->has('status'))
                        <div class="alert alert-success">{{ session('status') }}</div>
                    @else

                    <p><h4 class="text-center">Выбрано товаров: {{ $count or  0 }}, на сумму: {{ $sum or 0 }} Руб. </h4></p><br/>


                    <p><h5 class="text-center">Для оформления заказа заполните форму.</h5></p>
                    <p><h5 class="text-center"> Мы свяжемся с вами в ближайшее время.</h5></p>

                    <div class="login-form">
                        <form class="form-horizontal" role="form" method="POST" action="" onsubmit="yaCounter44026404.reachGoal('oformitpokupku'); return true;">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Ваше Имя</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Ваш E-Mail</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('telephone_number') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Телефон для связи</label>

                                <div class="col-md-6">
                                    <input id="telephone_number" type="text" class="form-control" name="telephone_number" value="{{ Auth::user()->telephone_number }}" required>

                                    @if ($errors->has('telephone_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('telephone_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                                <label for="comment" class="col-md-4 control-label">Комментарий к заказу</label>

                                <div class="col-md-6">
                                    <input id="comment" type="text" class="form-control" name="comment" placeholder="Необязатнльно к заполнению">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Оформить покупку
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    @endif

                </div>



            </div>
        </div>
    </section>
@endsection