<title>{{ $categorySelected->ceo_title or $categorySelected->name }}</title>
<meta name="description" value="{{ $categorySelected->ceo_description or config_path('app.ceo_description') }}">
<meta name="keywords" value="{{ $categorySelected->ceo_keywords or config('app.ceo_keywords') }}">
@extends('layouts.app')
@section('header_bottom')
    @include('layouts.header_bottom')
@endsection
@section('content')
    <section>
        <div class="container">
            <div class="row">
                @include('layouts.category')
                <div class="col-sm-9 padding-right">
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">{{ $categorySelected->name }}</h2>
                        @include('layouts.products')
                        <div class="title text-center">{{ $products->links() }}</div>
                    </div><!--features_items-->
                </div>
            </div>
        </div>
    </section>
@endsection