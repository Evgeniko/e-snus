@extends('layouts.app')
@section('header_bottom')
    @include('layouts.header_bottom')
@endsection
@section('content')
    <section>
        <div class="container">
            <div class="row">
                @include('layouts.category')

                <div class="col-sm-9 padding-right">
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">Весь ассортимент</h2>
                        @include('layouts.products')
                        <div class="title text-center">{{ $products->render() }}</div>
                    </div><!--features_items-->
                </div>
            </div>
        </div>
    </section>
@endsection