@extends('layouts.app')
@section('content')
    @include('admin.layouts.admin_header_bottom')
<div class="container">
@foreach($comments as $comment)
    <div class="panel panel-warning col-md-3">
        <a href="/admin/comment/delete/{{$comment->id}}" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></a>
        <div class="panel-heading">
            <div>
                {{ $comment->user->name }}
                <span class="text-success">{{ $comment->created_at->diffForHumans() }}</span>
            </div>
            <div>
                <a class="text-danger" href="/product/{{ $comment->product->id }}">
                    {{ $comment->product->name }}
                </a>
            </div>
        </div>
        <div class="panel-body">
            {{ $comment->content }}
        </div>
    </div>
@endforeach
</div>
@endsection