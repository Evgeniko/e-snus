@extends('layouts.app')
@section('content')
    @include('admin.layouts.admin_header_bottom')
    @include('admin.layouts.category')
    @include('admin.layouts.products')
    <div class="title text-center">{{ $products->render() }}</div>
@endsection