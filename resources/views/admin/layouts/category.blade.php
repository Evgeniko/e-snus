<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#adm-category">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="mainmenu pull">
                <ul class="nav navbar-nav collapse navbar-collapse" id="adm-category">
                    <li class="dropdown"><a href="/">Категории<span class="caret"></span></a>
                        <ul role="menu" class="sub-menu">
                            @foreach($categories as $category)
                                <li>
                                    <a href="/admin/category/{{$category->id}}" class="{{$category->status == 1? 'text-success' : 'text-danger'}}">
                                        {{$category->name}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
            @if(isset($categorySelected))
            <form class="form-horizontal" method="POST" action="/admin/category/edit/{{$categorySelected->id}}">
                {{csrf_field()}}
                <input name="name" value="{{$categorySelected->name}}">
                <label for="status">Доступность(отображение)</label>
                <label for="s-r-b-y">Да</label>
                <input id="s-r-b-y" name="status" type="radio" value="1" {{$categorySelected->status?"checked" : ""}}>
                <label for="s-r-b-n">Нет</label>
                <input id="s-r-b-n" name="status" type="radio" value="0" {{$categorySelected->status?"" : "checked"}}>
                <input type="submit" class="btn btn-default" value="Сохранить">
                <label for="check_ava">всё доступно</label>
                <input id="check_ava" name="check_availability" type="checkbox" value="true" />
                <div class="panel panel-default panel-no-margin">
                    <div class="panel-heading adm-panel-heading">
                        <div class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#seo-{{$categorySelected->id}}">
                                SEO
                            </a>
                        </div>
                    </div>
                    <div class="panel-group collapse" id="seo-{{$categorySelected->id}}">
                        <div class="panel panel-default panel-no-margin">
                            <div class="panel-heading adm-panel-heading">
                                <div class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#title-{{$categorySelected->id}}">
                                        ceo_title
                                    </a>
                                </div>
                            </div>
                            <div id="title-{{$categorySelected->id}}" class="panel-collapse collapse">
                                <textarea name="ceo_title" rows="3">{{$categorySelected->ceo_title}}</textarea>
                            </div>
                        </div>
                        <div class="panel panel-default panel-no-margin">
                            <div class="panel-heading adm-panel-heading">
                                <div class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#keywords-{{$categorySelected->id}}">
                                        ceo_keywords
                                    </a>
                                </div>
                            </div>
                            <div id="keywords-{{$categorySelected->id}}" class="panel-collapse collapse">
                                <textarea name="ceo_keywords" rows="3">{{$categorySelected->ceo_keywords}}</textarea>
                            </div>
                        </div>
                        <div class="panel panel-default panel-no-margin">
                            <div class="panel-heading adm-panel-heading">
                                <div class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#ceo_description-{{$categorySelected->id}}">
                                        ceo_description
                                    </a>
                                </div>
                            </div>
                            <div id="ceo_description-{{$categorySelected->id}}" class="panel-collapse collapse">
                                <textarea name="ceo_description" rows="3">{{$categorySelected->ceo_description}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @endif
        </div>
    </div>
</div>