<div class="container">
@foreach($products as $product)
    <form class="form-horizontal" method="POST" role="form" action="/admin/product/save/{{$product->id}}">
        {{ csrf_field() }}
        <div class="col-xs-6 col-sm-3 product-card">
            <div class="product-image-wrapper panel panel-default">
                <div class="single-products">
                    <div class="productinfo text-center">
                        <img src="{!! $product->image !!}" alt="{{ $product->name }}" />
                        <h2><input name="price" value="{{ $product->price }}" руб.></h2>
                        <p>
                            <input value="{{ $product->name }}" name="name">
                        </p>
                        <p>
                            <select class="rating-select" name="rating">
                                <option value="1"{{$product->rating == 1? 'selected':''}}>1</option>
                                <option value="2"{{$product->rating == 2? 'selected':''}}>2</option>
                                <option value="3"{{$product->rating == 3? 'selected':''}}>3</option>
                                <option value="4"{{$product->rating == 4? 'selected':''}}>4</option>
                                <option value="5"{{$product->rating == 5? 'selected':''}}>5</option>
                            </select>
                        </p>
                        <div class="panel panel-default">
                            <label for="is_popular">Популярность(на главной)</label>
                            <label for="r-b-y">Да</label>
                            <input id="r-b-y" name="is_popular" type="radio" value="1" {{$product->is_popular?"checked" : ""}}>
                            <label for="r-b-n">Нет</label>
                            <input id="r-b-n" name="is_popular" type="radio" value="0" {{$product->is_popular?"" : "checked"}}>

                        </div>
                        <div class="panel panel-default">
                            <label for="availability">Доступность(отображение)</label>
                            <label for="a-r-b-y">Да</label>
                            <input id="a-r-b-y" name="availability" type="radio" value="1" {{$product->availability?"checked" : ""}}>
                            <label for="a-r-b-n">Нет</label>
                            <input id="a-r-b-n" name="availability" type="radio" value="0" {{$product->availability?"" : "checked"}}>
                        </div>

                        <div class="product_attrib_wrap">
                            <div class="product_attrib can_amount">
                                <input name="pouches" class="adm-inp-attr" value="{{ $product->pouches }}"><br><label>Пакетиков</label>
                            </div>
                            <div class="product_attrib nicotine">
                                <input name="nicotine" class="adm-inp-attr" value="{{ $product->nicotine }}"> мг/г<br><label>Никотин</label>
                            </div>
                            <div class="product_attrib weight">
                                <input name="wp" class="adm-inp-attr" value="{{$product->wp}}">г<br><label>Вес Пакетика</label>
                            </div>
                        </div>
                        <div class="panel panel-default panel-no-margin">
                            <div class="panel-heading adm-panel-heading">
                                <div class="panel-title">
                                    <a data-toggle="collapse" href="#description-{{$product->id}}">
                                        Описание
                                    </a>
                                </div>
                            </div>
                            <div id="description-{{$product->id}}" class="panel-collapse collapse">
                                <textarea id="description" name="description" rows="3">{{$product->description}}</textarea>
                            </div>
                        </div>
                        <div class="panel panel-default panel-no-margin">
                            <div class="panel-heading adm-panel-heading">
                                <div class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#seo-{{$product->id}}">
                                        SEO
                                    </a>
                                </div>
                            </div>
                            <div class="panel-group collapse" id="seo-{{$product->id}}">
                                <div class="panel panel-default panel-no-margin">
                                    <div class="panel-heading adm-panel-heading">
                                        <div class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#title-{{$product->id}}">
                                                ceo_title
                                            </a>
                                        </div>
                                    </div>
                                    <div id="title-{{$product->id}}" class="panel-collapse collapse">
                                        <textarea name="ceo_title" rows="3">{{$product->ceo_title}}</textarea>
                                    </div>
                                </div>
                                <div class="panel panel-default panel-no-margin">
                                    <div class="panel-heading adm-panel-heading">
                                        <div class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#keywords-{{$product->id}}">
                                                ceo_keywords
                                            </a>
                                        </div>
                                    </div>
                                    <div id="keywords-{{$product->id}}" class="panel-collapse collapse">
                                        <textarea name="ceo_keywords" rows="3">{{$product->ceo_keywords}}</textarea>
                                    </div>
                                </div>
                                <div class="panel panel-default panel-no-margin">
                                    <div class="panel-heading adm-panel-heading">
                                        <div class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#ceo_description-{{$product->id}}">
                                                ceo_description
                                            </a>
                                        </div>
                                    </div>
                                    <div id="ceo_description-{{$product->id}}" class="panel-collapse collapse">
                                        <textarea name="ceo_description" rows="3">{{$product->ceo_description}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-success" type="submit">Сохранить</button>
                    </div>
                    <div class="stickers">
                        <div class="{{ $product->stock == 'Под заказ'? 'adm-inp-ordering' : 'adm-inp-availability' }}">
                            <input name="stock" class="{{ $product->stock == 'Под заказ'? 'adm-inp-ordering' : 'adm-inp-availability' }}" value="{{ $product->stock }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endforeach
</div>