@extends('layouts.app')
@section('content')
@include('admin.layouts.admin_header_bottom')
<div class="container text-center">
    <div class="col-sm-9 col-sm-offset-3">
        <form method="POST" action="/admin/product/add" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group col-sm-7">
                <select name="category_id"  >
                    <option disabled selected>Выберите категорию</option>
                    <option value="1">имя</option>
                </select>
            </div>
            <div class="form-group col-sm-7">
                <label class="col-sm-5" for="name">Имя товара</label>
                <input name="name"  >
            </div>
            <div class="form-group col-sm-7">
                <label class="col-sm-5" for="price">Цена(руб.)</label>
                <input id="add-product-price" name="price"  >
            </div>
            <div class="form-group col-sm-10">
                <textarea name="description"  >Описание</textarea>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3 text-right" for="pouches">Пак-ов</label>
                <input class="col-sm-1" name="pouches"  >
                <label class="col-sm-3 text-right" for="nicotine">Никотин</label>
                <input class="col-sm-1" name="nicotine"  >
                <label class="col-sm-3 text-right" for="wp">Вес пак-а</label>
                <input class="col-sm-1" name="wp"  >
            </div>
            <div class="form-group col-sm-7">
                <label for="availability">Доступность</label>
                Да<input type="radio" name="availability" value="1" checked>
                Нет<input type="radio" name="availability" value="0">
            </div>
            <div class="form-group col-sm-7">
                <label for="availability">Приоритет в выдаче</label>
                Да<input type="radio" name="is_new" value="1">
                Нет<input type="radio" name="is_new" value="0" checked>
            </div>
            <div class="form-group col-sm-7">
                <label for="availability">Показывать на главной</label>
                Да<input type="radio" name="is_popular" value="1">
                Нет<input type="radio" name="is_popular" value="0" checked>
            </div>
            <div class="form-group col-sm-7">
                <label for="stock">Стикер</label>
                <input name="stock"  >
            </div>
            <div class="form-group col-sm-7">
                <textarea name="ceo_title"  >SEO заголовок</textarea>
            </div>
            <div class="form-group col-sm-7">
                <textarea name="ceo_keywords"  >SEO кейворды</textarea>
            </div>
            <div class="form-group col-sm-7">
                <textarea name="ceo_description"  >SEO описание</textarea>
            </div>
            <div class="form-group col-sm-6">
                <label for="image">Выберите изображение</label>
                <input type="file" name="image">
            </div>
            <div class="form-group col-sm-7">
                <label for="rating" class="control-label">Ваша оценка</label>
                <select id="rating-select" name="rating">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5" selected>5</option>
                </select>
            </div>
            <div class="col-sm-7">
                <button type="submit" class="btn btn-primary">Добавить</button>
            </div>
        </form>
    </div>
</div>
@endsection