@extends('layouts.app')
@section('header_bottom')
    @include('layouts.header_bottom')
@endsection
@section('content')
    <section>
        <div class="container">
            <div class="row">
                @include('layouts.category')
                <div class="col-sm-9 padding-right">
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">По запросу найдено</h2>
                        @if(isset($error))
                            <div class="alert alert-danger text-center">
                                {{$error}}
                            </div>
                        @else
                        @include('layouts.products')
                        <div class="title text-center"> {{ $products->links() }}</div>
                    </div><!--features_items-->
                </div>
                @endif
            </div>
        </div>
    </section>
@endsection