<div class="panel panel-default comments">
    <div class="panel-heading">Отзывы и комментарии</div>
    @if(session('comment-confirmed'))
        <div class="alert alert-success alert-dismissible fade in text-center col-md-12" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>{{ Session::get('comment-confirmed') }}</strong>
        </div>
    @endif
    @if(session('comment-delete'))
        <div class="alert alert-danger alert-dismissible fade in text-center col-md-12" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>{{ Session::get('comment-delete') }}</strong>
        </div>
    @endif
    @foreach($comments as $comment)
    <div class="comments">
        <div class="author">
            <div class="comment-author-name">
                {{ $comment->user->name }}
                                        <select class="rating-show">
                                            <option value="1"{{$comment->rating == 1? 'selected':''}}>1</option>
                                            <option value="2"{{$comment->rating == 2? 'selected':''}}>2</option>
                                            <option value="3"{{$comment->rating == 3? 'selected':''}}>3</option>
                                            <option value="4"{{$comment->rating == 4? 'selected':''}}>4</option>
                                            <option value="5"{{$comment->rating == 5? 'selected':''}}>5</option>
                                        </select>
            </div>
        </div>
        <div class="comment-content">
            {{ $comment->content }}
        </div>
        <div class="text-muted">{{ $comment->updated_at->diffForHumans() }}</div>
        @can('delete-comment', $comment)
            <a class="text-muted" href="/comment/delete/{{$comment->id}}">Удалить</a>
        @endcan
    </div>
    @endforeach
    <div class="panel-body comment-form">
        @if( Auth::guest() )
            <div class="col-xs-12">
                <a href="/login" class="btn btn-danger btn-sm">войдите</a>
                или
                <a href="/register" class="btn btn-success btn-sm">зарегистрируйтесь</a> для возможности комментирования
            </div>
        @else
        <form class="form-horizontal" method="POST">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }} col-xs-12 col-md-6">
                <label for="name" class="control-label">Ваш комментарий</label>
                <textarea class="form-control" id="content" rows="2" name="content" required></textarea>
                @if ($errors->has('content'))
                    <span class="help-block">
                        <strong>{{ $errors->first('content') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="rating" class="control-label">Ваша оценка</label>
                <select id="rating-select" name="rating">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5" selected>5</option>
                </select>
            </div>
            <div class="form-group col-xs-12 col-md-10">
                    <button type="submit" class="btn btn-primary" id="comment-submit">
                        Отправить
                    </button>
            </div>
            @endif
        </form>
    </div>
</div>

@if(session()->has('comment-alert'))
    <div class="alert alert-success">
        {{ session('comment-alert') }}
    </div>
@endif