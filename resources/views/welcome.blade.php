@extends('layouts.app')
@section('header_bottom')
    @include('layouts.header_bottom')
@endsection
@section('content')
<section>
    <div class="container">
        @if(session()->has('register_confirmed'))
            <div class="alert alert-success alert-dismissible fade in text-center col-md-12" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong>{{ Session::get('register_confirmed') }}</strong>
            </div>
        @endif
        <div class="row">
            @include('layouts.category')

            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Рекомендуем</h2>
                    @foreach($popularProducts as $product)
                    <div class="col-xs-12 col-sm-4 product-card">
                        <div class="product-image-wrapper panel panel-default">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <a href="/product/{{ $product->id }}" >
                                        <img src="{!! $product->image !!}" alt="{{ $product->name }}" />
                                    </a>
                                    <h2>{{ $product->price }} руб.</h2>
                                    <p>
                                        <a href="/product/{{ $product->id }}" >
                                            {{ $product->name }}
                                        </a>
                                    </p>
                                    <p>
                                        <select class="rating-show">
                                            <option value="1"{{$product->rating == 1? 'selected':''}}>1</option>
                                            <option value="2"{{$product->rating == 2? 'selected':''}}>2</option>
                                            <option value="3"{{$product->rating == 3? 'selected':''}}>3</option>
                                            <option value="4"{{$product->rating == 4? 'selected':''}}>4</option>
                                            <option value="5"{{$product->rating == 5? 'selected':''}}>5</option>
                                        </select>
                                    </p>
                                    <div class="product_attrib_wrap">
                                        <div class="product_attrib can_amount">
                                            <strong>{{ $product->pouches }}</strong><br><label>Пакетиков</label>
                                        </div>
                                        <div class="product_attrib nicotine">
                                            <strong>{{ $product->nicotine }}</strong> мг/г<br><label>Никотин</label>
                                        </div>
                                        <div class="product_attrib weight">
                                            <strong>{{ $product->wp }}</strong> г<br><label>Вес/Пакетика</label>
                                        </div>
                                    </div>
                                    <a data-id="{{ $product->id }}"
                                       onclick="yaCounter44026404.reachGoal('vkorzinu'); return false;"
                                       class="btn btn-default btn-sm add-to-cart"><i class="fa fa-cart-plus" aria-hidden="true"></i>В корзину
                                    </a>
                                </div>
                                <div class="stickers">
                                    <div class="{{ $product->stock == 'Под заказ' ? 'ordering' : 'availability' }}">
                                        {{ $product->stock }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-xs-12 col-sm-12 text-center all-goods">
                        <a class="btn go-all" href="/catalog">Посмотреть весь ассортимент  <i class="fa fa-play" aria-hidden="true"></i></a>
                    </div>
                </div><!--features_items-->

                <!--<div class="recommended_items"> recommended_items
                    <h2 class="title text-center">наша группа вк</h2>

                    <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="col-sm-12">
                                    
                                </div>
                            </div>
                            <!--Вторая карусель
                            <div class="item">

                            </div>
                        <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a> --
                        </div>
                    </div><!--/recommended_items-->


                </div>
            </div>
        </div>
    </div>
</section>
    @stop
