@extends('layouts.app')
@section('header_bottom')
    @include('layouts.header_bottom')
@endsection
@section('content')

    @if( Session::has('saveSettings'))
        <div class="alert alert-success alert-dismissible fade in text-center col-md-10 col-md-offset-1" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>{{ Session::get('saveSettings') }}</strong>
        </div>
    @endif

    <div class="row" xmlns="http://www.w3.org/1999/html">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/home') }}">
                    {{ csrf_field() }}
                    <div class="panel-heading">Настройки</div>

                    <div class="panel-body">
                        <div class="form-group{{ $errors->has('trade_link') ? ' has-error' : '' }}">
                            <label for="trade_link" class="col-md-4 control-label">Ваше имя</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name"
                                       value="{{ Auth::user()->name ? Auth::user()->name:'' }}" required
                                />

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Ваш E-mail</label>
                            <div class="col-md-3">
                                <input id="email" type="text" class="form-control" name="email"
                                       value="{{ Auth::user()->email ? Auth::user()->email:'' }}" required
                                />

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group{{ $errors->has('telephone_number') ? ' has-error' : '' }}">
                            <label for="telephone_number" class="col-md-4 control-label">Телефон для связи</label>
                            <div class="col-md-6">
                                <input id="telephone_number" type="text" class="form-control" name="telephone_number"
                                       value="{{ Auth::user()->telephone_number? Auth::user()->telephone_number :'' }}"
                                />

                                @if ($errors->has('telephone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telephone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group accept-button">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Сохранить мои настройки
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
