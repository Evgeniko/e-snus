@extends('layouts.app')
@section('header_bottom')
    @include('layouts.header_bottom')
@endsection
@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="title text-center">Наши контакты</h2>
                <div class="well">
                    <div>Наименование организации: Swedish Tobacco LTD</div>
                    <div>Адрес склада: Израиль, г. Бейт-Шемеш, ул. Мича 13</div>
                    <div>Телефон (Израиль): +972 (2) 651-59-49</div>
                    <!--<div>Телефон(Россия, г. Чебоксары): +7-999-199-26-72 (<small>принимаем звонки в любое время</small>)</div>
                    <div>E-mail:  anythingrus@mail.ru</div>-->
                    <!--<a href="https://vk.com/club131855074">Наша группа вконтакте</a>-->

                    <br/>
                    <br/>
                    <div class="org__link org__link_type_map"><a class="link link__control i-bem link_js_inited" role="link" href="https://yandex.ru/maps/?ol=biz&amp;oid=1680161104&amp;ll=34.986308%2C31.736913&amp;z=15&amp;from=1org_map" title="Показать на карте" target="_blank" data-bem="{&quot;link&quot;:{}}"><img class="static-map" src="//static-maps.yandex.ru/1.x/?l=map&amp;ll=34.986308%2C31.736913&amp;size=300%2C140&amp;lang=ru_RU&amp;z=15&amp;pt=34.986308%2C31.736913%2Cpm2rdl" width="300" height="140"></a></div>
                    <br>
                    <div>
                        <h5>Дополнительные контакты (для жителей Украины, Казахстана, Кыргызстана, респ. Беларусь, России)<br>
						<br>+7(900)332-79-73
						<br>anythingrus@mail.ru
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
