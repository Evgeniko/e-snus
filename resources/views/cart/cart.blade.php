@extends('layouts.app')
@section('header_bottom')
    @include('layouts.header_bottom')
@endsection
@section('content')
<section>
    <div class="container">


        <div class="col-sm-8 padding-right col-sm-offset-2">
            <div class="features_items">
                <h2 class="title text-center">Корзина</h2>

                @if ($products)
                <p>Вы выбрали такие товары:</p>
                <table class="table-bordered table-striped table">
                    <tr>
                        <th>Товар</th>
                        <th>Цена<p>в Руб.</p></th>
                        <th>Кол-во<p>(шт.)</p></th>
                        <th>Удалить</th>
                    </tr>
                    @foreach ($products as $product)
                    <tr>
                        <td>
                            <a href="/product/{{ $product->id }}">
                                {{ $product->name }}
                            </a>
                        </td>
                        <td>{{ $product->price }} руб.</td>
                        <td>{{ $product->pivot->count }}</td>
                        <td>
                            <a href="/cart/delete/{{ $product->id }}">
                                <i class="fa fa-times"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="3">Общая стоимость:</td>
                        <td>{{ $sum }} Руб.</td>
                    </tr>

                </table>
                @if($count == 0)
                    <button class="btn btn-default disabled" data-toggle="tooltip" data-placement="right" title="Нет товаров для оформления"><i class="fa fa-shopping-cart"></i> Оформить заказ</button>
                @else
                    <a class="btn btn-default checkout" onclick="yaCounter44026404.reachGoal('oformitzakaz')" href="/cart/order"><i class="fa fa-shopping-cart"></i> Оформить заказ</a>
                @endif

                @else
                <p><h3>Корзина пуста</h3></p>

                <a class="btn btn-default checkout" href="/"><i class="fa fa-shopping-cart"></i> Вернуться к покупкам</a>
                @endif

            </div>



        </div>
    </div>
</section>
@endsection