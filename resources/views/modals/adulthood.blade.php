<div class="modal fade" id="adulthood" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">Просмотр данного сайта разрешен только лицам, достигшим возраста <b>18 лет</b></h4>
            </div>
            <div class="modal-body text-center" >
                <strong>
                <p>
                    Нажав на кнопку «Продолжить» Вы подтверждаете, что Вам исполнилось 18 лет, принимаете <a href="/agreement">условия пользовательского соглашения</a>и  выражаете свое требование на демонстрацию изображений табачных изделий.
                </p>
                <!--</p>
                    (в соответствии с требованиями ст. 19 Федерального закона от 23.02.2013 года N 15-ФЗ "Об охране здоровья граждан от воздействия окружающего табачного дыма и последствий потребления табака".)
                </p>
				-->
                </strong>
            </div>
            <div class="modal-footer text-center">
                <button id="goYandex" class="btn btn-danger pull-left">Выйти</button>
                <button id="adulthood-accept" class="btn btn-success" onclick="yaCounter44026404.reachGoal('voititasait');">Продолжить</button>
            </div>
        </div>
    </div>
</div>