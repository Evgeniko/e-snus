<div class="modal fade" id="notAuthModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">УПС!</h4>
            </div>
            <div class="modal-body text-center" >
                <strong>
                    Мы оказываем услуги только зарегистрированным пользователям!<br><br>
                    Для начала вам нужно <a href="/login" class="btn btn-danger btn-sm"   onclick="yaCounter44026404.reachGoal('vojtiprinelogin');">войти</a>
                    или
                    <a href="/register" class="btn btn-success btn-sm" onclick="yaCounter44026404.reachGoal('zaregatsyaprinelogin');">зарегистрироваться</a>
                </strong>
            </div>
            <div class="modal-footer text-center">
            </div>
        </div>
    </div>
</div>