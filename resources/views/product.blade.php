<title>{{ $product->ceo_title or $product->name }}</title>
<meta name="description" value="{{ $product->ceo_description or $product->description }}">
<meta name="keywords" value="{{ $product->ceo_keywords or config('app.ceo_keywords') }}">
@extends('layouts.app')
@section('header_bottom')
    @include('layouts.header_bottom')
@endsection
@section('content')
    <section>
        <div class="container">
            <div class="row">
                @include('layouts.category')

                <div class="col-sm-9 padding-right">
                    <div class="product-details"><!--product-details-->
                        <div class="row">
                            <h2 class="title text-center">{{ $product->name }}</h2>
                            <div class="col-xs-8 col-xs-offset-2 col-sm-5 col-sm-offset-0">
                                <div class="view-product">
                                    <img src="{!! $product->image !!}" alt="{{ $product->name }}" />
                                </div>
                                <p class="text-center">
                                    <select class="rating-show">
                                        <option value="1"{{$product->rating == 1? 'selected':''}}>1</option>
                                        <option value="2"{{$product->rating == 2? 'selected':''}}>2</option>
                                        <option value="3"{{$product->rating == 3? 'selected':''}}>3</option>
                                        <option value="4"{{$product->rating == 4? 'selected':''}}>4</option>
                                        <option value="5"{{$product->rating == 5? 'selected':''}}>5</option>
                                    </select>
                                </p>
                            </div>
                            <div class="col-xs-12 col-sm-7">
                                <div class="product-information"><!--/product-information-->
                                    <div class="stickers">
                                        <div class="{{ $product->stock == 'Под заказ'? 'ordering' : 'availability' }}">
                                            {{ $product->stock }}
                                        </div>
                                    </div>
                                    <h2>{{ $product->name }}</h2>
                                    <span>
                                        <span>{{ $product->price }}руб.</span>
                                        <label>Количество:</label>
                                        <input  id="count" type="text" value="1" />
                                        <a onclick="yaCounter44026404.reachGoal('vkorzinu'); return false;" data-id="{{ $product->id }}"
                                           class="btn btn-default add-to-cart cart"><i class="fa fa-shopping-cart"></i>В корзину
                                        </a>
                                        <br><br>
                                        <p><b>Порционность:</b> {{ $product->pouches }}</p>
                                        <p><b>Содержание никотина мг/г:</b> {{ $product->nicotine }}</p>
                                        <p><b>Вес однго пакетика:</b> {{ $product->wp }}</p>
                                    </span>
                                </div><!--/product-information-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h5>Описание товара</h5>
                                {!! $product->description !!}
                            </div>
                        </div>
                    </div>
                    <!--/product-details-->
                    <!--Блок комментариев-->
                    @include('comment.comment')
                </div>
            </div>
        </div>
    </section>
@endsection