/**
 * Created by EVG on 23.03.2017.
 */
$('.rating-show').barrating({
    theme: 'fontawesome-stars',
    deselectable: true,
    readonly: true
});

$('.rating-select').barrating({
    theme: 'fontawesome-stars',
    deselectable: true,
});

$('#rating-select').barrating({
    theme: 'fontawesome-stars',
    initialRating: 5,
    deselectable: true
});

$(document).ready(function(){



    /*Проверка на совершенолетие*/
    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }
    if (!getCookie('adulthood')){
        if (window.location.pathname == '/agreement'){
            return false;
        }
        $('#adulthood').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    }

    //Установка кук после подтверждения возраста
    $('#adulthood-accept').on('click', function () {
        var date = new Date(new Date().getTime() + 1000*60*60*24*365);
        document.cookie = "adulthood=true; path=/; expires=" + date.toUTCString();
        $('#adulthood').modal('hide');
    });

    //Переход на яндекс,если не подтверждён возраст
    $('#goYandex').on('click', function () {
        document.location.href = 'https://www.google.ru/?gfe_rd=cr&ei=TBLlWOWTJIzCNO7Colg#newwindow=1&q=%D0%B7%D0%B4%D0%BE%D1%80%D0%BE%D0%B2%D1%8B%D0%B9+%D0%BE%D0%B1%D1%80%D0%B0%D0%B7+%D0%B6%D0%B8%D0%B7%D0%BD%D0%B8&*';
    });

    //Всплывающее уведомление на кнопку
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    // Запрет на ввод символов,кроме чисел
    $('#count, #telephone_number, #add-product-price').bind("change keyup input click", function() {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
    });

    /* ===========AJAX ЗАПРОСЫ===========*/

    //AJAX запрос на добавление товара в корзину
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".add-to-cart").click(function () {
        var id = $(this).attr("data-id");
        var count = 1;
        if($("*").is("#count")) {
            count = $("#count").val();
        }

        $.post("/cart/add/"+id+"/"+count, {}, function (data) {
            if(data.error !== undefined){
                alert('Упс! Вам нужно войти, либо зарегестрироваться');
                return;
            }
            $("#cart-count").html("("+data+")");
            $.notify("Выбранный вами товар был добавлен в корзину<br> " +
                "Перейти <a class='btn btn-danger' href='/cart'>В корзину</a>", {
                x:50,
                y:100,
                type:'success'
            });
        })
        .fail(function () {
            $('#notAuthModal').modal('show');
        });
    });

    //AJAX на добавление комментария
    $("#comment-submit").on('click', function () {
        var content = '';
    });


});