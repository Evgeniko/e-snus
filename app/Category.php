<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    public $timestamps = false;

    protected $fillable = [
        'name', 'sort_order', 'status'
    ];

    public function scopeAvailability($query)
    {
        return $query->where('status', 1);
    }

    public function product()
    {
        return $this->hasMany('App\Product');
    }
}
