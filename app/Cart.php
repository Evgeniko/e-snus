<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Cart extends Model
{
    protected $table = 'cart';

    protected $fillable = [
        'user_id', 'product_id', 'count'
    ];

    public $timestamps = false;

    public function getCount()
    {
        return $this->where(['user_id', 'count' ], Auth::id())->sum('count');
    }
}
