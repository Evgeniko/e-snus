<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;

class ComposerViewServiceProvider extends ServiceProvider
{
    public function boot()
    {
        \Illuminate\Support\Facades\View::composer(
            ['layouts.header_bottom', 'order.orderForm', 'cart.cart'], 'App\Http\ViewComposers\LayoutAppComposer'
        );
    }
}