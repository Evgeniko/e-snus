<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Product extends Model
{
    use Searchable;

    protected $table = 'goods';

    public $timestamps = false;

    protected $guarded = [
        'id'
    ];

    public function user()
    {
        return $this->belongsToMany('App\User', 'cart');
    }
    
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function coments()
    {
        return $this->hasMany('App\Comment');
    }
}
