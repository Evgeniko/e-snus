<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    /*
     * Массив разрешённых полей к массовому заполнению
     */
    protected $fillable = [
      'user_id', 'products', 'status'
    ];
    /**
     * Relationship User model
     */
    public function users()
    {
        $this->belongsTo('App\User');
    }
}
