<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;

class Comment extends Model
{
    /*
    |Трейт для замены стандартного Carbon
    |на локализированный под рус.язык
     */
    use LocalizedEloquentTrait;

    protected $fillable = [
        'user_id', 'product_id', 'content', 'rating'
    ];

    /*
     *
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
