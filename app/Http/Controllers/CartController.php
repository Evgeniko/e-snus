<?php

namespace App\Http\Controllers;

use App\Cart;
use App\User;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show cart page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $products = User::find(Auth::id())->products()->get();
        $sum = $products->sum(function ($product){
            return $product->price * $product->pivot->count;
        });

        return view('cart.cart', compact('products', 'sum'));
    }

    /**
     * Ajax метод
     * Возвращает кол-во продуктов в корзине для моментального обновления счётчика корзины
     * Get count products for user
     *
     * @return mixed
    **/
    public function getCount()
    {
        return Cart::where('user_id', Auth::id())->sum('count');
    }

    /**
     * Add product in cart for user
     *
     * @param $id
     * @param int $count
     * @return mixed
     */
    public function add($id, $count = 1)
    {

        Cart::updateOrCreate(
            ['user_id' => Auth::id(), 'product_id' => $id],
            ['user_id' => Auth::id(), 'product_id' => $id]
        )->increment('count', $count);
        return $this->getCount();
    }

    /**
     * Delete product in cart for user
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        Cart::where('product_id', $id)->delete();
        return back();
    }

}
