<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = new CategoryController;
        $categoryList = $category->getCategory();

        return view('home',compact('categoryList'));
    }

    /**
     * Save profile settings
     *
     * @param Request $request
     */
    public function saveSettings(Request $request)
    {
        $user = User::find(Auth::id());
        $user->name = $request->name;
        $user->email = $request->email;
        $user->telephone_number = $request->telephone_number;
        $user->save();
        Session::flash('saveSettings', 'Ваши настройки сохранены!');

        return back();
    }

}
