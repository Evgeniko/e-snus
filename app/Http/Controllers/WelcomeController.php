<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WelcomeController extends Controller
{

    /**
     * Show main page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categoryList = Category::availability()->get();
        $popularProducts = Product::where('is_popular', 1)->get();
        return view('welcome',compact('categoryList', 'popularProducts'));
    }

}

