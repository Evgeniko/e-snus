<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Get all category products
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCategory()
    {
        $categoryList = Category::all();
        return $categoryList;
    }

    /**
     * Get products from selected category
     *
     * @param $categoryId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProductsByCategoryId($categoryId)
    {
        $categoryList = Category::availability()->get();
        $products = Product::where([
            ['category_id','=', $categoryId],
                ['availability', '=', 1]
        ])->paginate(9);
        $categorySelected = Category::find($categoryId);
        if ($categorySelected->status){
            return view('products.category', compact('categoryList', 'products', 'categorySelected'));
        }
        //Ошибка 404 если текущая категория отмечена недоступной
        abort(404);
    }

    /**
     * Get all products
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAllProducts()
    {
        $categoryList = Category::availability()->get();
        $products = Product::paginate(9);
        return view('products.allProduct', compact('categoryList', 'products'));
    }
    
}
