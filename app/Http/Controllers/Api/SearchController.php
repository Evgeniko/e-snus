<?php

namespace App\Http\Controllers\Api;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Support\Facades\Session;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $error = "Увы!По вашему запросу ничего не найдено";
        $categoryList = Category::availability()->get();
        // Удостоверимся, что поисковая строка есть
        if($request->has('query')) {
            // Используем синтаксис Laravel Scout для поиска по таблице products.
            $products = Product::search($request->get('query'))->where('availability','1')->paginate(9);
            //return $products->count() ? $products : $error;
            if ($products->count()){
                return view('foundProducts', compact('categoryList','products'));
            }
            //error
            return view('foundProducts', compact('categoryList','products', 'error'));
        }

        // Вернем сообщение об ошибке, если нет поискового запроса
        //error
        return view('foundProducts', compact('categoryList','products', 'error'));
    }
}
