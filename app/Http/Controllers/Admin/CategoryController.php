<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Edit category by id
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function edit(Request $request, $id)
    {
        Category::where('id', $id)->update([
            'name' => $request->name,
            'status' => $request->status,
            'ceo_title' => $request->ceo_title,
            'ceo_keywords' => $request->ceo_keywords,
            'ceo_description' => $request->ceo_description
        ]);
        if ($request->status == 0){
            Product::where('category_id', $id)->update([
               'availability' => 0
            ]);
        }
        if ($request->status == 1 && $request->check_availability){
            Product::where('category_id', $id)->update([
                'availability' => 1
            ]);
        }
        Product::all()->searchable();
        return back();
    }
}
