<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{

    public function getByCategoryId($id)
    {
        $categories = Category::all();
        $products = Product::where('category_id', $id)->paginate(20);
        $categorySelected = $categorySelected = Category::find($id);
        return view('admin.category_products', compact('products', 'categories', 'categorySelected'));
    }

    /**
     * Add product in DB
     *
     * @param Request $request
     */
    public function add(Request $request)
    {
        $file = $request->file('image');
        $filename = $file->getClientOriginalName();
        $file->move("images/goods/".$request->category_id, $filename);
        $request = $request->toArray();
        $request['image'] = "/images/goods/".$request['category_id']."/$filename";
        Product::create($request);
        Session::flash('add-status', 'Новый товар был успешно добавлен!');
        return back();
    }

    /**
     * Show add product form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAddForm()
    {
        return view('admin.addProduct');
    }

    /**
     * Edit product by id
     *
     * @param Request $request
     * @param $id
     */
    public function edit(Request $request, $id)
    {
        $request = $request->toArray();
        unset($request['_token']);
        Product::where('id', $id)->update($request);
        return back();
    }

}
