<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    public function show()
    {
        //$comments = DB::table('comments')->orderBy('id', 'desc')->get();
        $comments = Comment::with('product', 'user')->orderBy('id', 'desc')->get();
        return view('admin.comments', compact('comments'));
    }

    /**
     * Delete comment by id
     *
     * @param $id
     */
    public function delete($id)
    {
        Comment::destroy($id);
        return back();
    }

    /**
     * Edit comment by id
     *
     * @param $id
     */
    public function edit($id)
    {
        //
    }


}
