<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdministrationController extends Controller
{
    public function dashboard()
    {
        $categories = Category::all();
        $products = Product::paginate(20);
        return view('admin.dashboard', compact('products','categories'));
    }
}
