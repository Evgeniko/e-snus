<?php

namespace App\Http\Controllers;

use App\Mail\OrderShipped;
use App\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Show order form
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function showOrderForm(Request $request)
    {
        $products = User::find(Auth::id())->products()->get();
        $sum = $products->sum(function ($product){
            return $product->price * $product->pivot->count;
        });
        return view('order.orderForm', compact('sum'));
    }

    /**
     * Delete from cart and insert for orders table
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function order(Request $request)
    {
        $products = User::find(Auth::id())->products()->get();
        $sum = $products->sum   (function ($product){
            return $product->price * $product->pivot->count;
        });

        Validator::make($request->all(),[
            'name' => 'required|min:2',
            'email' => 'required'

        ])->validate();

        DB::transaction(function (){
            $userCart = DB::table('cart')->select('product_id', 'count')->where('user_id', Auth::id())->get()->toArray();
            $order['user_id'] = Auth::id();
            foreach ($userCart as $key=>$value){
                $order['products'][$value->product_id] = $value->count;
            }
            $order['status'] = 0;
            $order['products'] = serialize($order['products']);
            $order['created_at'] = Carbon::now();
            $order['updated_at'] = Carbon::now();
            DB::table('orders')->insert($order);
            DB::table('cart')->where('user_id', Auth::id())->delete();
        },3);

        $orderProducts = "Товары : \n";
        foreach ($products as $product){
            $orderProducts .= $product->name." - ".$product->price."руб. : ".$product->pivot->count."шт.|||".$_SERVER['HTTP_HOST']."/product/$product->id \n";
        }
        $adminEmail = env('MAIL_ADMIN');
        $subject = 'Возможно новый заказ от'.$request->name;
        $body_message = 'От господина: '.$request->name."\n";
        $body_message .= 'E-mail или ссылка ВК: '.$request->email."\n";
        $body_message .= 'Номер : '.$request->telephone_number."\n";
        $body_message .= 'Комментарий: '.$request->comment."\n";
        $body_message .= 'На общую сумму: '.$sum."руб\n";
        $body_message .= $orderProducts;
        mail($adminEmail, $subject, $body_message);

        Session::flash('status', 'Заказ оформлен. Мы свяжемся с Вами в ближайшее время.');
        return redirect('cart/order');

    }

}
