<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class CommentController extends Controller
{

    /**
     * Save user comment
     *
     * @param Request $request
     */
    public function save(Request $request, $id)
    {
        $request['user_id'] = Auth::id();
        $request['product_id'] = $id;
        Comment::create($request->toArray());
        Session::flash('comment-confirmed', 'Комментарий успешно добавлен!');
        return back();
    }

    /**
     * Delete user comment by id
     *
     * @param $id
     */
    public function delete($id)
    {
        $comment = Comment::find($id);
        if (Gate::allows('delete-comment', $comment)){
            Comment::destroy($id);
        }
        Session::flash('comment-delete', 'Ваш комментарий успешно удалён!');
        return back();
    }

}
