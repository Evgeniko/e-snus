<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Get popular products
     *
     * @return mixed

    public function getPopularProducts()
    {
        $popularProducts = Product::where('is_popular', 1)->take(9)->get();
        return $popularProducts;
    }
     */
    /**
     * Get product by id
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProductById($id)
    {
        $categoryList = Category::availability()->get();
        $product = Product::where('availability', 1)->findOrFail($id);
        $comments = Comment::with('user')->where('product_id', $id)->get();
        return view('product', compact('categoryList', 'product', 'comments'));
    }
}
