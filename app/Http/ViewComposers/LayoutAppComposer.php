<?php

namespace App\Http\ViewComposers;


use App\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class LayoutAppComposer
{
    /**
     * Sen $count in selected views
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('count', Cart::where('user_id', Auth::id())->sum('count'));
    }
}