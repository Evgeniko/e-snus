<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::post('/home', 'HomeController@saveSettings');

Route::get('/product/{id}', 'ProductController@getProductById');
Route::get('/catalog', 'CategoryController@getAllProducts');
Route::get('/category/{id}', 'CategoryController@getProductsByCategoryId');

Route::group(['middleware' => 'cart'], function (){
    Route::get('/cart', 'CartController@index');
    Route::get('/cart/delete/{id}', 'CartController@delete')
                ->where(['id' => '[0-9]+']);
    Route::get('/cart/order', 'OrderController@showOrderForm');
    Route::post('/cart/order', 'OrderController@order')->middleware(['order']);

    Route::post('product/{id}', 'CommentController@save');
    Route::get('comment/delete/{id}', 'CommentController@delete');
});

Route::get('/search', 'Api\SearchController@search');

Route::group(['prefix' => 'admin'], function (){
    Route::group(['middleware' => 'admin'], function (){
        Route::get('/', 'Admin\AdministrationController@dashboard');
        Route::get('/category/{id}', 'Admin\ProductController@getByCategoryId');
        Route::post('/category/edit/{id}', 'Admin\CategoryController@edit');
        Route::post('/product/save/{id}', 'Admin\ProductController@edit');
        Route::get('/product/add', 'Admin\ProductController@showAddForm');
        Route::post('/product/add', 'Admin\ProductController@add');
        Route::get('/comments', 'Admin\CommentController@show');
        Route::get('/comment/delete/{id}', 'Admin\CommentController@delete');
    });

});

Route::post('/cart/add/{id}/{count?}', 'CartController@add')
        ->where(['id' => '[0-9]+', 'count' => '[0-9]+']);

Route::get('/about', function (){
    return view('pages.about');
});

Route::get('/contacts', function (){
    return view('pages.contacts');
});

Route::get('/agreement', function (){
    return view('pages.userAgreement');
});

